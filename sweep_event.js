define([
	'require',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/controls/controls',
	'madjoh_modules/styling/styling'
],
function(require, CustomEvents, Controls, Styling){
	var SweepEvent = {
		sweepReactivity : 1000,
		minimalMove : 0.05,

		addTouchListener : function(component){
			component.className += ' touchListened';
			component.touchListener = {};
			component.addEventListener(Controls.pressed, SweepEvent.touchStart, false);
			component.addEventListener(Controls.released, SweepEvent.touchEnd, false);
		},
		touchStart : function(e){
			var source = e.target;
			while(source.className.indexOf('touchListened')<0) source = source.parentNode;

			var sweepSource;
			if(Controls.pressed == 'mousedown') sweepSource = e;
			else sweepSource = e.touches[0];
			source.touchListener.xStart = sweepSource.clientX;
			source.touchListener.yStart = sweepSource.clientY;
			source.touchListener.tStart = new Date();
		},
		touchEnd : function(e){
			var source = e.target;
			while(source.className.indexOf('touchListened')<0) source = source.parentNode;

			if(source.touchListener.tStart){
				tEnd = new Date();
				var dt = tEnd.getTime()-source.touchListener.tStart.getTime();

				if(dt<SweepEvent.sweepReactivity){
					var sweepSource;
					if(Controls.pressed == 'mousedown') sweepSource = e;
					else sweepSource = e.changedTouches[0];

					var dx = sweepSource.clientX - source.touchListener.xStart;
					var dy = sweepSource.clientY - source.touchListener.yStart;
					if(Math.abs(dx) > Styling.getWidth()*SweepEvent.minimalMove ||Math.abs(dy) > Styling.getWidth()*SweepEvent.minimalMove){
						if(Math.abs(dx)>2*Math.abs(dy)){
							if(dx>0) CustomEvents.fireCustomEvent(source, 'sweepTouchRight');
							else CustomEvents.fireCustomEvent(source, 'sweepTouchLeft');
							CustomEvents.fireCustomEvent(source, 'sweepTouch');
						}else if(Math.abs(dy)>2*Math.abs(dx)){
							if(dy>0) CustomEvents.fireCustomEvent(source, 'sweepTouchBottom');
							else CustomEvents.fireCustomEvent(source, 'sweepTouchTop');
							CustomEvents.fireCustomEvent(source, 'sweepTouch');
						}
					}	
				}
			}

			source.touchListener = {};
		}
	};

	return SweepEvent;
});
	