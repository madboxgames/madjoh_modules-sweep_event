# sweep_event #

v1.0.3

This module defines the sweep event over a DOM object.

## Getting Started ##

All you need to do is to use SweepEvent.addTouchListener() over your DOM element.
You can then listen to the following custom events :

- ** sweepTouch ** : A sweep action has been detected.
- ** sweepTouchLeft ** : A sweep action to the Left has been detected.
- ** sweepTouchRight ** : A sweep action to the Right has been detected.
- ** sweepTouchTop ** : A sweep action to the Top has been detected.
- ** sweepTouchBottom ** : A sweep action to the Bottom has been detected.

### Example ###
```html
<!DOCTYPE html>
<html>
	<head>[...]</head>
	<body>
		[...]
			<div id="sweepMe"></div>
		[/...]
	</body>
</html>
```

```js
var SweepEvent = require('SweepEvent');
var CustomEvents = require('CustomEvents');

var element = document.getElementById('sweepMe');
SweepEvent.addTouchListener(element);

var greets = function(){
	alert('coucou');
}

CustomEvents.addCustomEventListener(element, 'sweepTouchLeft', greets);
```